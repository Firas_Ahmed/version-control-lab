#include <iostream>
using namespace std;

int main(){
  float x, y;
  cout << "Please enter 2 numbers:";
  cin >> x >> y;
  
  cout << "The sum of " << x << " and " << y
       << " is: " <<  x + y << endl;
  cout << "The difference of " << x << " and " << y
       << " is: " <<  x - y << endl;
  cout << "The product of " << x << " and " << y
       << " is: " <<  x * y << endl;
  cout << "The distance of " << x << " and " << y
       << " is: " <<  abs(x-y) << endl;
  cout << "The mean of " << x << " and " << y
       << " is: " << (x + y)/2  << endl;
  
}
