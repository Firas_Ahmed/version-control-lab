#include <iostream>
using namespace std;
int main() {
    char c;
    cout << "Enter a character: ";
    cin >> c;

    if(c == 'A' || c == 'E' || c == 'I' || c == 'O' || c == 'U' || c  == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u'){
        cout << c << " is a Vowel\n";
    }else if((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z')){
        cout << c << " is a Consonant\n";
    }else if(c >= '0' && c <= '9'){
        cout << c << " is a Digit\n";
    }else if(c == '.' || c == ',' || c == ':' || c == ';' || c == '?' || c == '"' || c == '!' || c == '_' || c == '-' || c == '/'){
        cout << c << " is a Punctuation\n";
    }else{
        cout << c << " is Unrecognised\n";
    }
}
